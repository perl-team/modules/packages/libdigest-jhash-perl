libdigest-jhash-perl (0.10-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ gregor herrmann ]
  * Update 'DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow' to '=+all'.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 16:25:52 +0100

libdigest-jhash-perl (0.10-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nick Morrott ]
  * Import upstream version 0.10
  * debian/control: declare compliance with Debian policy 3.9.8
  * debian/upstream/metadata: refresh metadata
  * debian/rules: enable hardening=+bindnow

 -- Nick Morrott <knowledgejunkie@gmail.com>  Tue, 29 Nov 2016 13:28:39 +0000

libdigest-jhash-perl (0.09-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Add debian/upstream/metadata.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Nov 2015 17:03:52 +0100

libdigest-jhash-perl (0.08-1) unstable; urgency=low

  * Team upload.

  [ Angel Abad ]
  * New upstream release 0.07
  * Bump Standards-Version to 3.9.1 (no changes)
  * Add myself to Uploaders

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release 0.08.
  * debian/copyright: update Upstream-Contact.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Drop unused build dependencies.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Feb 2014 16:15:41 +0100

libdigest-jhash-perl (0.06-1) unstable; urgency=low

  * Initial Release (Closes: #591877).

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Mon, 09 Aug 2010 18:50:49 -0430
